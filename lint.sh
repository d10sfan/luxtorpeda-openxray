#!/bin/bash

find_shellscripts () {
	{
		echo bootstrap/update
		ls ./*.sh
		# shellcheck disable=SC2038
		find . -maxdepth 1 -type f \
			| xargs file --mime-type \
			| grep text/x-shellscript \
			| cut -d : -f 1
	} | sort -u
}

readonly shellscripts=$(find_shellscripts)

set -x

# shellcheck disable=SC2086
shellcheck $shellscripts

pylint bootstrap/update_package.py

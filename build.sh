#!/bin/bash

source env.sh

set -x
set -e

readonly pstart="$PWD"
readonly pfx="$PWD/local"
mkdir -p "$pfx"

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

export CC=/usr/bin/gcc-8
export CXX=/usr/bin/g++-8
export SystemDrive="$pfx"
PATH="/usr/lib/binutils-2.30/bin:$PATH"

# build freeimage
pushd "freeimage"
make -j "$(nproc)"
make -f Makefile.fip -j "$(nproc)"
mkdir -p "$pfx/include"
mkdir -p "$pfx/lib"
cp -rfv "Dist/libfreeimage-3.18.0.so" "$pfx/lib"
cp -rfv "Dist/FreeImage.h" "$pfx/include"
cp -rfv "Dist/libfreeimageplus-3.18.0.so" "$pfx/lib"
cp -rfv "Dist/FreeImagePlus.h" "$pfx/include"
popd

# build lockfile
pushd "liblockfile"
mkdir -p "$pfx/LockFile"
./configure --enable-shared --prefix="$pfx/LockFile"
make -j "$(nproc)"
make install
popd

# build crypto++
pushd "source/Externals/cryptopp"
mkdir -p "$pfx/Crypto++"
make clean
make dynamic -j "$(nproc)"
make install PREFIX="$pfx/Crypto++"
cp -rfv "$pfx/Crypto++/include/cryptopp"/*  "$pfx/Crypto++/include"
popd

# build lzo
pushd "source/Externals/lzo"
chmod +x configure
./configure --enable-shared --prefix="$pfx"
make -j "$(nproc)"
make install
popd

# build readline
pushd "readline"
./configure --enable-shared --prefix="$pfx"
make -j "$(nproc)"
make install
popd

# build tbb
pushd "tbb"
mkdir -p build
cd build
cmake \
	-DCMAKE_PREFIX_PATH="$pfx" \
	..
make -j "$(nproc)"
make install
popd

# build glew
pushd glew-2.1.0
GLEW_DEST="$pfx" make -j "$(nproc)"
GLEW_DEST="$pfx" make install
popd

# build openxray
pushd "source"

mkdir -p bin
cd bin

cmake \
        -DFREEIMAGE_LIBRARY="$pfx/lib/libfreeimage-3.18.0.so" \
        -DFREEIMAGEPLUS_LIBRARY="$pfx/lib/libfreeimageplus-3.18.0.so" \
        -DLZO_ROOT_DIR="$pfx" \
        -DCMAKE_PREFIX_PATH="$pfx" \
        -DTBB_INSTALL_DIR="$pfx" \
        -DTBB_INCLUDE_DIRS="$pfx/include" \
        -DTBB_LIBRARY_DIRS="$pfx/lib" \
        -DFREEIMAGE_INCLUDE_PATH="$pfx/include" \
        -DGLEW_INCLUDE_DIRS="$pfx/include" \
        -DGLEW_LIBRARIES="$pstart/glew-2.1.0/lib" \
        -DGLEW_USE_STATIC_LIBS=ON \
        ..
make -j "$(nproc)"
make install

popd

mkdir -p "41700/dist/lib"
mkdir -p "41700/dist/bin"

cp -rfv "/usr/local/share/openxray"/* "41700/dist/"
cp -rfv /usr/local/lib/xr*.so "41700/dist/lib/"
cp -rfv "/usr/local/bin/xr_3da" "41700/dist/"
cp -rfv "/usr/local/lib/OPCODE.so" "41700/dist/lib/"
cp -rfv "/usr/local/lib/libODE.so" "41700/dist/lib/"
cp -rfv "$pfx/lib"/*.so* "41700/dist/lib/"
cp -rfv "$pfx/Crypto++/lib"/*.so* "41700/dist/lib"
cp -rfv "$pstart/tbb/build"/libtbb*.so* "41700/dist/lib"
cp -rfv "$pstart/liblockfile/liblockfile.so" "41700/dist/lib"
pushd "41700/dist/lib"
ln -s "liblockfile.so" "liblockfile.so.1"
popd
cp -rfv glew-2.1.0/lib/*.so* "41700/dist/lib/"
cp openxray-cop.sh "41700/dist/"

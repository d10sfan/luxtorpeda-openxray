#!/usr/bin/python3

# pylint: disable=missing-docstring

import os
import readline
import subprocess
import string


def query_user(prompt, text):
    def hook():
        readline.insert_text(text)
        readline.redisplay()
    readline.set_pre_input_hook(hook)
    result = input(prompt)
    readline.set_pre_input_hook()
    return result


def fill_template(src, dest):
    with open(src, 'r') as template, open(dest, 'w') as out:
        text = string.Template(template.read())
        out.write(text.safe_substitute(globals()))


def template_list(fmt):
    return '\n'.join(fmt(a) for a in STEAM_APP_ID_LIST.split())


def anchor_br(url, txt):
    return f'<a href="{url}">{txt}</a><br>'


PACKAGE_NAME = query_user('package name: ', os.path.basename(os.getcwd()))
PREV_APP_ID_LIST = os.environ.get('STEAM_APP_ID_LIST', '123450')
STEAM_APP_ID_LIST = query_user('steam app_ids: ', PREV_APP_ID_LIST)


DOWNLOAD_LINKS = template_list(
        lambda a: anchor_br(f'{PACKAGE_NAME}-{a}.tar.xz', f'download ({a})'))
ARTIFACT_DIRS = template_list(
        lambda a: f'    - {a}/')
ARTIFACT_TARBALLS = template_list(
        lambda a: f'    - {a}/dist.tar.xz')
MV_TARBALLS_TO_PUBLIC = template_list(
        lambda a: f'  - mv {a}/dist.tar.xz public/{PACKAGE_NAME}-{a}.tar.xz')

print('+ create index.html')
fill_template('bootstrap/templates/index.html', 'index.html')
print('+ create env.sh')
fill_template('bootstrap/templates/env.sh', 'env.sh')
print('+ create .gitlab-ci.yml')
fill_template('bootstrap/templates/gitlab-ci.yml', '.gitlab-ci.yml')
for app_id in STEAM_APP_ID_LIST.split():
    app_id_json = f'manifests/{app_id}.json'
    if not os.path.isfile(app_id_json):
        print('+ create', app_id_json)
        fill_template('bootstrap/templates/app_id.json', app_id_json)
# print(f'+ git submodule add {PROJECT_URL}')
# subprocess.call(['git', 'submodule', 'add', PROJECT_URL, 'source'])
print('+ git status')
subprocess.call(['git', 'status'])
